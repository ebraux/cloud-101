
# Introduction au Cloud

La documentation générée est disponible sur le site [https://ebraux.gitlab.io/cloud-101/](https://ebraux.gitlab.io/cloud-101/)


## Test en local des slides

Génération du PDF
```bash
cd docs/slides
mkdir output
chmod 777 output
docker run -it --rm  --init -v ${PWD}:/home/marp/app/ -e LANG=$LANG marpteam/marp-cli cloud-101.md --allow-local-files --pdf -o output/cloud-101.pdf
```

Watch mode
```bash
docker run --rm --init -v $PWD:/home/marp/app/ -e LANG=$LANG -p 37717:37717 marpteam/marp-cli -w cloud-101.md
```

Server mode (Serve current directory in http://localhost:8080/)
```bash
docker run --rm --init -v $PWD:/home/marp/app -e LANG=$LANG -p 8080:8080 -p 37717:37717 marpteam/marp-cli -s .
```


## Tests en local du site


Build de l'image
```bash
docker build -t mkdocs_cloud-101 .
```
rem : 
Creation du site
```bash
docker run  -v ${PWD}/..:/work mkdocs_cloud-101 mkdocs new
```


Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_cloud-101 mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_cloud-101 mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_cloud-101 rm -rf /work/site
docker image rm mkdocs_intro-cloud
```


rem : Creation du site
```bash
docker run  -v ${PWD}/..:/work mkdocs_cloud-101 mkdocs new
sudo chown -R $(id -u -n):$(id -g -n)  *
```

