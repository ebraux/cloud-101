---
marp: true
paginate: true
theme: default
---

# Cloud à la réunion

| Société | Services |
| ------- | -------- |
| Exodata | Hébergement, Sauvegarde, Office 365, **Iaas** (VmWare), **Paas**, VDI |
| NXO | Réseau, Stockage, Sauvegarde, **IaaS**, **BaaS**, **DRaaS** | 
| Stor | Datacenter, Hébergement, Sauvegarde, Stockage, Infogérance |
| Youtell | Datacenter, Hébergement, Infogérance, Réseau, Office 365 |


---

| Société | Services |
| ------- | -------- |
| HODI  | Hébergement, Messagerie |
| MDSI | Hébergement, Messagerie, |
| colombia-reunion | Hébergement, Messagerie, VDI  |
| Itoi | Hébergement, Sauvegardes | 
| GigaRun| Hébergement, Office 365 |

---
# Liens

- [https://www.exodata.fr/cloud/itaas](https://www.exodata.fr/cloud/itaas)
- [https://www.nxo.eu/qui-sommes-nous/nos-filiales/nxo-ocean-indien/](https://www.nxo.eu/qui-sommes-nous/nos-filiales/nxo-ocean-indien/)
- [https://www.stor.fr/hebergement-cloud/](https://www.stor.fr/hebergement-cloud/)
- [https://www.youtell.re/service/hebergement-de-serveurs/](https://www.youtell.re/service/hebergement-de-serveurs/)


---

- [https://hodi.host/re/datacenter/](https://hodi.host/re/datacenter/)
- [https://mdsi.re/services/cloud-computing/](https://mdsi.re/services/cloud-computing/)
- [https://colombia-reunion.re/cloud-hebergement/](https://colombia-reunion.re/cloud-hebergement/)
- [https://www.itoi.fr/cloud-hebergement/](https://www.itoi.fr/cloud-hebergement/)
- [https://www.gigarun.re/](https://www.gigarun.re/)
    - [https://demo.gigarun.net/](https://demo.gigarun.net/)
    - [https://www.gigarun.re/informatique/solutions/cloud-computing-entreprise.html](https://www.gigarun.re/informatique/solutions/cloud-computing-entreprise.html)