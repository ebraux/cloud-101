---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #0074d0 -->
<!-- _color: white -->
![bg left:40% 100%](assets/cloud.png)

# Introduction Au Cloud

## Impact du Cloud

-emmanuel.braux@imt-atlantique.fr-

---

# Licence informations


Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---

# Impact Technique

## Ne rien changer

- Cher, Limité

## Adapter l'existant

- peut être un bon compromis.

## Nouvelle architecture

* Démarche d'architecture orientée valeure, et pilotée par la mesure
* Construire l'architecture, la faire évoluer en continu
* **Pratiquement repartir de zéro**

---

# Impact RH

## Sur les équipes

- en SaaS, plus besoin de développeurs
- en PaaS, plus besoin d'ingénieurs système
- en IaaS, réduction des équipes

## Sur les compétences

- Nouveaux métiers : architectes Cloud, acheteurs de services, ...
- Nouvelles façons de travailler: Agile, Devops, ...

---

# Impact Financier

## Nouveau modèle

- de l'investissement (CAPEX)
- à la location (OPEX)

## Paiement à la demande

- quels contrôles ?
- quelles limites ?


---

# Impact Légal

Quel droit s'applique ?

Les données

- Où sont vos données ?
- Qui en est propriétaire ?
- Comment les récupérer ?
- Quelles protections ?


