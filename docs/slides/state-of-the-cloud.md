---
marp: true
paginate: true
theme: default
---
<!-- _backgroundColor: #0074d0 -->
<!-- _color: white -->
![bg left:40% 100%](assets/cloud.png)

# Introduction Au Cloud

## "State of the Cloud"

-emmanuel.braux@imt-atlantique.fr-

---

# Licence informations


Auteur : emmanuel.braux@imt-atlantique.fr

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
# Report "State of the Cloud"

Rapport annuel sur l'usage du cloud : 
[https://info.flexera.com/CM-REPORT-State-of-the-Cloud](https://info.flexera.com/CM-REPORT-State-of-the-Cloud)


- Sondage mondial, auprès de 753 "cloud decision-makers and users", fin 2023.
- Type d'organisations :
    

---
# Aspects stratégiques et fonctionnels

- La grande majorité des entreprises s'appuient sur le multi-cloud, notamment "Multiple public + private"
- La problématique du coût prend une place de plus en plus importante (FinOps)
- L'approche du cloud n'est pas uniquement IT, mais globale
- Les aspects sécurité et gouvernance sont au coeur des préoccupations
- Les utilisateurs de cloud public sont principalement aux état unis. Mais également en Europe et en Asie. L'utilisation en Afrique est toujours très peu développée.

---
# Aspects techniques

- La migration dans le cloud nécessite la maîtrise des architectures applicatives
- La plupart des entreprises s'appuient sur des fournisseurs de services
- L'utilisation des containers est devenue un standard, celle de PaaS progresse de façon significative.
- AWS et Azure sont au coude à coude, GCP est toujours en position d'outsider mais loin derrière. Ils devancent très largement la concurrence

---
# Salariés

![bg left:60% 90%](img/report/enterprise-size.png)

- < 1 000  : SMBs
- \> 1 000  : Enterprises
- \> 10 000  : Large Enterprises
---
# Secteur d'activité

![h:500](img/report/enterprise-sector.png)

---
# Siège social

![](img/report/worldmap.png)

---
# Maturité "déclarée" dans l'usage du Cloud

![](img/report/maturity.png)

---
# Utilisation Multi cloud

![h:500](img/report/multicloud.png)

---
# Type de Multicloud

![h:500](img/report/multicloud-strategy.png)

---
# Utilisation cloud privé/public

![h:500](img/report/private-public.png)


---
# Providers de cloud Public 

![h:550](img/report/provider-evolution.png)

---
# Type d'usage chez les différents providers

![](img/report/provider.png)

---
# Technologies pour les cloud privés

![](img/report/private-technologies.png)


---
# Budget Cloud : évolution et dépenses gaspillées
![h:300 ](img/report/spend-evol.png) ![h:280](img/report/spend-waste.png)

---
# Services Utilisés

![](img/report/services.png)

---
# Objectifs de l'utilisation du cloud
![](img/report/challenges.png)

---
# Démarche Développement Durable et empreinte carbone

![w:1800](img/report/carbon.png)

---
# Utilisation pour de l'IA générative

![w:1800](img/report/generative-ia.png)



