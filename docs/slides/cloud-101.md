---
marp: true
paginate: true
theme: default
---
<!--  #0074d0 
 le marron-->
<!-- #ff5001 le orange -->
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:40% 100% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](assets/cloud.png)



# Cloud 101

## Introduction au Cloud et Principes de base

-emmanuel.braux@imt-atlantique.fr-

---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](assets/cloud.png)

# Introduction Au Cloud

## Définition 

## Approche "Service"

## A la demande, En libre service

## Types de Cloud

## Interêts du Cloud

---

# Licence informations


Auteur : -emmanuel.braux@imt-atlantique.fr-

Cette présentation est sous License Créative Commons 3.0 France (CC BY-NC-SA 3.0 FR)
Selon les options : Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

![cc-by-nc-sa](img/cc-by-nc-sa.png)

---
<!-- _backgroundColor:  #0074d0  -->
<!-- _color: white -->
![bg left:30% 80%](assets/cloud.png)

# Introduction Au Cloud

## Définition

---

# Définition

Wikipédia (*extraits*) : "Le cloud computing  ...

- Exploitation *partagée* de la puissance de **calcul** ou de **stockage** de **serveurs** informatiques **distants**  par l’intermédiaire d’un **réseau**, généralement l’internet.

- Mise à disposition sous forme de **service** 
  
- Accès  **à la demande** et  **en libre-service**

- **Adaptation automatique** à la demande de la **capacité** ... selon le besoin du consommateur

*[https://fr.wikipedia.org/wiki/Cloud_computing](https://fr.wikipedia.org/wiki/Cloud_computing)*

---
# NIST cloud model

![bg left:55% 95% ](img/NIST-Visual-Model-of-Cloud-Computing-a-Service-Models-Cloud-computing-can-be-classified.png)

5 caractéristiques essentielles
3 modèles de service
4 modèles de déploiement.

*[Source: visual model of NIST working definition of cloud computing](https://www.researchgate.net/figure/NIST-Visual-Model-of-Cloud-Computing-a-Service-Models-Cloud-computing-can-be-classified_fig2_241195178)*

---

## Un nouveau modèle orienté **Services**

## Un nouveau mode de consommation : **En libre service, à la demande, flexible**

## Une facturation en fonction de **l'usage réel**

## Des technologies

- Beaucoup de technologies qui existaient déjà
- Des nouvelles technologies, qui petit à petit sont adoptées hors du Cloud

---

<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](assets/cloud.png)

# Introduction Au Cloud

## Approche "Service"

---

## Accès à des services en ligne  

- Puissance de calcul, capacité de stockage,  fonctionnalités réseau, …
- Outils, Applications, environnement de travail
- ...

## Changement de modèle

- On n'achète plus un serveur.
- On loue un environnement d'execution pour une durée donnée, avec un niveau de service associé.

---
# Analogie avec le monde réél

Acheter une voiture deviendrait **souscrire à un service de déplacement**, adaptable aux usages :

- une trotinette ou des basquettes pour aller à la cantine
- une petite voiture pour aller en ville
- une grande voiture pour partir en vacances
- un camping car pour les week-end
- ...

Impossible en réél, mais possible en virtuel.

---
# Les "familles" de services

## Principalement

- **IaaS** Infrastructure as a Service : déployer des infrastructures
- **PaaS** Platform as a Service : construire des services
- **SaaS** Software as a Service : consommer des services

## Mais aussi

- Database as a Service (DBaaS), Desktop as a Service (Daas), Backend as a Service (Baas), Fonctions as a Service (Faas), Conteneur as a Service (Caas), Disaster Recovery as a Service (DRaaS), ...

---

# SaaS : Software As A Service

- L’utilisateur final n’a plus besoin d’installer l’application sur son poste
- Il accède à son compte  et aux applications par le Web
- Les applications sont utilisées dans le  cadre d’un abonnement,
- Les données peuvent aussi être stockées sur un serveur de l’opérateur SaaS.

- Exemples d’utilisation :
  - Collaboration : messagerie, gestion de projet,  …
  - Métier : CRM, comptabilité …

*Microsoft Office 365, Google Apps, FaceBook, Skype, NetFlix, DropBox, SlideShare …*

---

# PaaS : Platform As A Service

- Fourniture d’une plateforme, permettant de déployer un service
- Pour un environnement précis :  un langage / un framework (Python, Java, PHP, ...)
- Pas besoin d'expertise système pour pouvoir développer
- Sécurisation et Mises à jours de l'environnement pris en charge
- Principalement utilisé par des développeurs, pour déployer leurs applications avec des commandes simples sans se soucier de la partie serveur.

- *Exemples de Paas*: 
  - *Publics : Amazon Elastic Beanstalk, Google App Engine, Heroku, ...*
  - *Privés : OpenShift (Red Hat), Cloud Foundry, ...*

---

# IaaS : Infrastructure As A Service

- Infrastructures de bas niveau : Stockage, machines virtuelles, réseau, ...
- Dans la plupart des cas virtualisées
- Datacenter sécurisés, infrastructures mondiales : optimisation du traffic, redondance, ...
- Louées à la demande : pas d'investissement
- L’utilisateur peut disposer au besoin d’une capacité de traitement pour n’importe quel type d’application.

*Exemple : AWS, Azure, GCP, OVH Cloud, DigitalOcean, ...*

---

# Qu'est qu'un service

## C'est une application ou une ressource
## + un engagement contractuel

- Disponibilité
- Sécurité
- Support
- Qualité
- Confidentialité
- Conformité
- ...

---

# Les niveaux de responsabilité

![bg left:30% 65% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/responsability-detail.png)

- Les Utilisateurs
  - Besoin d'un outil fiable, et performant
  - Les données ...
- Les Developpeurs (DEV)
  - Besoin d'environnements de travail souples, adaptables, réactifs et évolutifs 
- Les Opérateurs (OPS)
  - Du matériel : serveurs, datacenters, réseau, ...
  - De la stablité, de la sécurité
  - Des normes et contraintes

---

# Modéle de responsabilité

![h:580 ](img/responsability-model.png)

---

![bg contain](img/Pizza_as-a_Service.png)

---

<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](assets/cloud.png)

# Introduction Au Cloud

## A la demande, En libre service

---
# En mode « libre-service »

## Avantages

- Accès **simple** : notion de catalogue
- **Rapidité** de mise en service
- **Autonomie** : pas de formulaires, pas de demande d'autorisation,...

## Inconvénients

- **Montée en compétences** des utilisateurs
- **Responsabilisation** sur les aspects sécurité, cycle de vie des ressource, ...

---
# A la demande

## Avantages

- Flexibilité et élasticité
- Facturation à l’usage
- Plus proche des besoins rééls

## Inconvénients

- Responsablisation sur les usages : quels contrôles, quelles limites ?
- Quel modèles de facturation : mode "pot commun",  re-facturation ?

---

<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](assets/cloud.png)

# Introduction Au Cloud

## Types de Cloud

---

# Cloud Privé

![bg left:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/ressources-cloud-private.png)

- Dédié à l'entreprise
- Investissements d’infrastructure
- Equipes formées et disponibles
- Principaux avantages
    - Autonomie
    - Souveraineté
- Principaux inconvénients
    - Délais
    - Complexité
    - Ressources internes

---

# Cloud Public

![bg left:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/ressources-cloud-public.png)

- Services proposés en location par un fournisseur externe (AWS, Rackspace, OVH, etc.)
- Aucun besoin d’infrastructure dans l’entreprise.
- Partagés avec d'autre organisations
- Principaux avantages : Flexibilité, Réactivité, Adaptabilité, coût
- Principaux inconvénients: Souveraineté, Coût

---

# Cloud Hybride

![bg left:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/ressource-cloud-hybride.png)

Combinaison de Cloud Privé et Cloud Public

- Pic de charge
- services spécifiques

---

# Multicloud

![bg left:40% 90% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](img/multicloud.png)
Plusieurs Cloud Publics

- Redondance
- Type de service
- Niveau de service
- Coût

![multicloud](img/multicloud-acteurs.png)

---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](assets/cloud.png)

# Introduction Au Cloud

## Intérêts du Cloud

---
# Avantages mis en avant

- Diminution du budget matériel et logiciel
- Pas de maintenance nécessaire
- Meilleurs performances
- Infrastructure indépendante, toujours disponible, depuis n'importe où, à n'importe quelle heure
- Meilleure gestion des mises à jour des applications
- Capacités de stockage illimité
- Amélioration de la sécurité des données

---
# Dans la réalité

- Coûts : 
    - Beaucoup de coup cachés, et de mauvaises surprise
    - Tendance cloud hybride voir retour vers le "on premise"
- Disponibilité, performances :
    - Quelques rares pannes de Datacenter
    - Le cloud fonctionne trop bien : les "design pattern" d'architecture ne sont pas toujours respectées

---
# Aspects non techniques

- Stratégies / Objectifs Internes
- Empreinte énergétique
- Vendor Lock-in
- Time to Market

---
# Rapport annuel  "State of the Cloud"
## Aspects stratégiques et fonctionnels

- La grande majorité des entreprises s'appuient sur le multi-cloud, notamment "Multiple public + private"
- La problématique du coût prend une place de plus en plus importante (FinOps)
- L'approche du cloud n'est pas uniquement IT, mais globale
- Les aspects sécurité et gouvernance sont au coeur des préoccupations
- Les utilisateurs de cloud public sont principalement aux Etats unis. Mais également en Europe et en Asie. L'utilisation en Afrique est toujours très peu développée.

*[https://info.flexera.com/CM-REPORT-State-of-the-Cloud](https://info.flexera.com/CM-REPORT-State-of-the-Cloud)*

---
# Rapport annuel  "State of the Cloud" 
## Aspects techniques

- La migration dans le cloud nécessite la maîtrise des architectures applicatives
- La plupart des entreprises s'appuient sur des fournisseurs de services
- L'utilisation des containers est devenue un standard, celle de PaaS progresse de façon significative.
- AWS et Azure sont au coude à coude, GCP est toujours en position d'outsider mais loin derrière. Ils devancent très largement la concurrence


*[https://info.flexera.com/CM-REPORT-State-of-the-Cloud](https://info.flexera.com/CM-REPORT-State-of-the-Cloud)*

---

# Quelques problématiques liées Cloud public

- Cohérence du Système d'Information : "Shadow IT", coordination, intégration, ...
- Les données : propriété, protection, ...
- Impact sur les équipes internes : que deviennent les développeurs, les ingés système, les techniciens...
- Les nouvelles compétences : juridiques, achat de service
- Passage du modèle "investissement" (CAPEX) à "location" (OPEX) 
- Sécurité 
- ...

---
<!-- _backgroundColor:  #0074d0 
 -->
<!-- _color: white -->
![bg left:30% 80% drop-shadow:0,5px,10px,rgba(0,0,0,.4)](assets/cloud.png)

# Introduction Au Cloud

## Conclusion

---

# L'utilisation du Cloud a un impact global sur les entreprises

- Nouvelle approche orientée **valeur**
- Nouvelles **architectures** construites autour des **services**
- Nouveaux **métiers** : architectes Cloud, acheteurs de services, ...
- Nouvelles **organisations**, nouvelles **pratiques**: Agile, Devops, ...



