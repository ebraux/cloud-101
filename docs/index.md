# Introduction au Cloud

Slides de la présentation:

* Version en ligne: [cloud-101.html](slides/cloud-101.html)
* version PDF: [cloud-101.pdf](slides/cloud-101.pdf)


Slides complémentaires:

* Impact du Cloud
    * Version en ligne: [cloud-impact.html](slides/cloud-impact.html)
    * version PDF: [cloud-impact.pdf](slides/cloud-impact.pdf)
* State of the Cloud
    * Version en ligne: [state-of-the-cloud.html](slides/state-of-the-cloud.html)
    * version PDF: [state-of-the-cloud.pdf](slides/state-of-the-cloud.pdf)
 
---

![image alt <>](assets/cloud.png)

